# RSVP System
RSVP System is an Open Source project focused on building flexible reservation and event systems with Drupal CMS.

Use cases include Event Calendars, Room Reservations, Team Schedule, Equipment Loans, and other date-based applications. This project can be used to build advance calendar functionalities, with content and access segragation based around a central Community Taxonomy.

# [Recipe Cookbook](https://gitlab.com/rsvp-system/rsvp-recipes)
Our collection of custom Drupal Recipes. Fields, Taxonomies, Content Types, Report Views, URL Path Auto patterns, User Shortcuts, ... and more!

# [Module Kit](https://gitlab.com/rsvp-system/rsvp-system-module)
A a small custom module for extra things that might not fit within a Drupal Recipe. 

# [Web Documentation](https://gitlab.com/rsvp-system/rsvp-system) | [Composer Template](https://github.com/slackstone/rsvp_composer/tree/main) 
A small documentation and example repository, plus Composer templates and eample snippets. 

## RSVP System Recipe Highlights

**Community Taxonomy**: a taxonomy attached as reference field to site Content Types. Used to organize and scure content within site sections". Basic components for Name, Community Description, Logos, colors variables are stored for vocabulary items.

**Community Type Taxonomy**: A reference taxonomy for assigning a Type to the Community. This taxonomy helps define site sections by providing a "Short Name" field for URL tokens and path patterns.   Examples include: Neighborhoods, Departments, Product Lines, Blog Topics, Community Partners, etc.

**Resource Taxonomy**: A content type designed for managing detailed information about reservable resources.

**Resource Type Taxonomy:**  Examples include: Room Location, Equipment Loan, Office Inventory, Entertainment Media Library

**Request**: A content type used for submitting reservation details.

**Log/Transaction**: (TBD) A content type for logging all activities related to reservation requests. This includes recording outcomes like approvals, declines, and cancellations, serving as a historical record to maintain data integrity and provide insights into reservation trends and outcomes.

**Feed Imports**: Importers for Community, Community Tyope (Taxonomy), Locations, Events, Resources (Content)
